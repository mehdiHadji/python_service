import logging
import os

import tensorflow as tf
from flask import Flask, request, json, jsonify
from keras.models import load_model

from src.entities.ClientEntitie import Client
from src.entities.ClientEntitieCleaner import ClientCleaner
from src.entities.MockEntitie import Data
from src.entities.UserEntitie import User
from src.entities.UserEntitieCleaner import UserCleaner
from src.exceptions.Exceptions import AttributeTypeMismatch
from src.helpers.functions import load_input, transform_input, predict

tf.get_logger().setLevel('ERROR')
os.environ["KMP_WARNINGS"] = "FALSE"

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.logger.setLevel(logging.ERROR)

IMAGE_LENGTH = 128
MODEL_PATH = '/src/pkl_objects/model.hdf5'

graph = None
classifier = None


def load_model_():
    global graph
    global classifier
    import sys
    print("######## SYS", sys.path)
    classifier = load_model(filepath=MODEL_PATH)
    classifier._make_predict_function()
    graph = tf.get_default_graph()


@app.route("/")
def home():
    return jsonify(isError=False, message="Success", statusCode=200, data="API IS WORKING"), 200


@app.route('/create_user', methods=['POST'])
def create_user():
    try:
        body = request.get_json()
        data = Data(**body)
        response = {"msg": "user created with success !", "user_data": json.loads(data.toJson())}
        return jsonify(isError=False, message="Success", statusCode=200, data=response), 200

    except AttributeTypeMismatch as e:
        app.logger.error(str(e))
        return jsonify(isError=True, message="Failure", statusCode=400, data=str(e)), 400

    except Exception as e:
        app.logger.error(str(e))
        return jsonify(isError=True, message="Failure", statusCode=400, data=str(e)), 400


@app.route('/process_user', methods=['POST'])
def process_user():
    try:
        body = request.get_json()
        data = User(**body)
        data = UserCleaner(data).transform()
        response = {"msg": "user processed with success !", "user_data": json.loads(data.toJson())}
        return jsonify(isError=False, message="Success", statusCode=200, data=response), 200

    except AttributeTypeMismatch as e:
        app.logger.error(str(e))
        return jsonify(isError=True, message="Failure", statusCode=400, data=str(e)), 400

    except Exception as e:
        app.logger.error(str(e))
        return jsonify(isError=True, message="Failure", statusCode=400, data=str(e)), 400


@app.route('/create_client', methods=['POST'])
def create_client():
    try:
        body = request.get_json()
        data = Client(**body)
        response = {"msg": "user created with success !", "user_data": json.loads(data.json())}
        return jsonify(isError=False, message="Success", statusCode=200, data=response), 200

    except Exception as e:
        app.logger.error(str(e))
        error_trace = json.loads(e.json()) if e.__class__.__name__ == 'ValidationError' else str(e)
        return jsonify(isError=True, message="Failure", statusCode=400, traceback=error_trace), 400


@app.route('/process_client', methods=['POST'])
def process_client():
    try:
        body = request.get_json()
        data = Client(**body)
        data = ClientCleaner(data).transform()
        response = {"msg": "user created with success !", "user_data": json.loads(data.json())}
        return jsonify(isError=False, message="Success", statusCode=200, data=response), 200

    except Exception as e:
        app.logger.error(str(e))
        error_trace = json.loads(e.json()) if e.__class__.__name__ == 'ValidationError' else str(e)
        return jsonify(isError=True, message="Failure", statusCode=400, traceback=error_trace), 400


@app.route('/checkNudityV2', methods=['POST'])
def checkNudityV2():
    try:
        received_input = load_input(request.get_json()['url'])
        transformed_input = transform_input(input_image=received_input, img_length=IMAGE_LENGTH)

        # src: https://github.com/keras-team/keras/issues/10431
        with graph.as_default():
            result = predict(input_classifier=classifier, input_entry=transformed_input)

        return jsonify(isError=False, message="Success", statusCode=200, prediction=result), 200

    except Exception as e:
        app.logger.error(str(e))
        return jsonify(isError=True, message="Failure", statusCode=400, traceback=str(e)), 400


if __name__ == "__main__":
    load_model_()
    app.run(host='0.0.0.0', port=5001, debug=True)
