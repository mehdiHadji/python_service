import re
from datetime import date

from pydantic import BaseModel, validator

from src.entities.CredictCardEntitie import Card
from src.exceptions.Exceptions import PhoneNumberFormatException, EmailFormatException
from src.globals.class_validator import RegexPattern


class Client(BaseModel):
    first_name: str
    last_name: str
    birth_date: date
    gender: str
    email: str
    age = 0
    is_eligible = False
    country: str
    country_code = ""
    country_name = ""
    phone: str
    card: Card

    @validator('phone')
    def phone_number_validation(cls, gsm_: str):
        if not re.match(RegexPattern.PHONE_NUMBER.value, gsm_):
            raise PhoneNumberFormatException()
        return gsm_

    @validator("email")
    def email_validation(cls, email_: str):
        if not re.match(RegexPattern.EMAIL.value, email_):
            raise EmailFormatException()
        return email_
