from src.cleaners.global_cleaners import capitalize_str, uppercase_str

from src.cleaners.specific_cleaners import (
    compute_age,
    infer_gender,
    is_eligible_age,
    extract_country_code,
    extract_country_name
)

from src.entities.UserEntitie import User


class UserCleaner:
    user: User 

    def __init__(self, user):
        self.user = user

    def transform(self) -> User:
        self.user.first_name = capitalize_str(self.user.first_name)
        self.user.last_name = uppercase_str(self.user.last_name)
        self.user.age = compute_age(self.user.birth_date)
        self.user.gender = infer_gender(self.user.gender)
        self.user.is_eligible = is_eligible_age(self.user.age)
        self.user.country_code = extract_country_code(self.user.country)
        self.user.country_name = extract_country_name(self.user.country)
        return self.user
