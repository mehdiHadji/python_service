import datetime
import json
import re

from src.encoders.json_datetime_encoder import DTEncoder
from src.exceptions.Exceptions import EmailFormatException, PhoneNumberFormatException
from src.validators.attributes_checker import auto_attr_check


@auto_attr_check
class User:
    first_name = str
    last_name = str
    birth_date = datetime.date
    gender = str
    email = str
    age = int
    is_eligible = bool
    country = str
    country_code = str
    country_name = str
    phone = str

    def __init__(self, first_name, last_name, birth_date, gender, email, country, phone):
        self.first_name = first_name
        self.last_name = last_name
        self.birth_date = datetime.datetime.strptime(birth_date, "%Y-%m-%d").date()
        self.gender = gender
        self.email = self._is_valid_email(email)
        self.age = 0
        self.is_eligible = False
        self.country = country
        self.country_code = ""
        self.country_name = ""
        self.phone = self._is_valid_phone_number(phone)

    def _is_valid_email(self, email: str) -> str:
        regex = "^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$"
        if not re.match(regex, email):
            raise EmailFormatException()
        return email

    def _is_valid_phone_number(self, gsm: str) -> str:
        """
        ~ Accepted ~
        0123456789              01 23 45 67 89          01.23.45.67.89          0123 45.67.89
        0033 123-456-789        0033(0)123456789        +33-1.23.45.67.89       +33 - 123 456 789
        +33(0) 123 456 789      +33 (0)123 45 67 89     +33 (0)1 2345-6789      +33(0) - 123456789

        ~ Rejected ~
        012345678               01234567890             0+123456789
        (0)123456789            01  23  45  67  89

        """
        regex = "^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$"
        if not re.match(regex, gsm):
            raise PhoneNumberFormatException()
        return gsm

    def __str__(self):
        return f"<<{self.first_name}::{self.last_name}::{self.last_name}::{self.birth_date}>>"

    def toDict(self) -> dict:
        return {
            "first_name": self.first_name,
            "last_name": self.last_name,
            "birth_date": self.birth_date,
            "gender": self.gender,
            "email": self.email,
            "age": self.age,
            "is_eligible": self.is_eligible,
            "country_code": self.country_code,
            "country_name": self.country_name,
            "phone": self.phone
        }

    def toJson(self):
        return json.dumps(self.toDict(), cls=DTEncoder)
