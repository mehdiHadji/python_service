def capitalize_str(val):
    if isinstance(val, str):
        return val.capitalize()
    else:
        return val


def uppercase_str(val):
    if isinstance(val, str):
        return val.upper()
    else:
        return val

