from datetime import date


def compute_age(birth_date: date) -> int:
    """
    :param birth_date: date
    :return: age in years
    """
    if isinstance(birth_date, date):
        today = date.today()
        return today.year - birth_date.year - ((today.month, today.day) < (birth_date.month, birth_date.day))
    else:
        return 0


def infer_gender(gender: str) -> str:
    genders = {"M": "Male", "F": "Female"}
    if isinstance(gender, str):
        return genders[gender]


def is_eligible_age(age: int) -> bool:
    if isinstance(age, int):
        if age >= 18:
            return True
        else:
            return False
    else:
        return False


def extract_country_code(country: str) -> str:
    if isinstance(country, str):
        return country[:3]
    else:
        return ""


def extract_country_name(country: str) -> str:
    if isinstance(country, str):
        return country[4:].upper()
    else:
        return ""




