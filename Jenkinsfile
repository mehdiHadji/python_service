def COMMIT_HASH

pipeline {
    agent any
    stages {

        stage('Setup SSH Tunnel') {
            when {
                branch 'master'
            }
            steps {
                script {
                    sh 'ssh -f -o ExitOnForwardFailure=yes -NL 2234:127.0.0.1:22 jenkins@2a01:cb00:38e:7500:a00:27ff:fee6:ca39 sleep 360'
                }
            }

        }

        stage('Build and push docker image') {
            steps {
                script {
                    COMMIT_HASH = sh(returnStdout: true, script: 'git rev-parse HEAD').trim().take(7)
                    dockerImage = docker.build("registry.gitlab.com/mehdihadji/python_service:latest", "-f ./Dockerfile .")
                }

                withCredentials([usernamePassword(
                    credentialsId: "registry", usernameVariable: "USER", passwordVariable: "PASS")]) {
                        sh "echo $PASS | docker login registry.gitlab.com -u $USER --password-stdin"
                }

                sh "docker tag registry.gitlab.com/mehdihadji/python_service:latest registry.gitlab.com/mehdihadji/python_service:${COMMIT_HASH}"
                sh "docker push registry.gitlab.com/mehdihadji/python_service:latest"
                sh "docker push registry.gitlab.com/mehdihadji/python_service:${COMMIT_HASH}"
            }

        }

        stage('Release') {
            when {
                expression { return (env.GIT_BRANCH =~ '.*master.*').matches() }
            }

            steps {
                releaseCurrentVersion()
            }
        }


        stage('Deploy') {
            when {
                expression { return (!(env.GIT_BRANCH =~ '^origin/.*').matches() && !(env.GIT_BRANCH =~ '.*master.*').matches()) || ((env.GIT_BRANCH =~ 'origin/develop').matches()) || ((env.GIT_BRANCH =~ 'origin/mlops_dev').matches())}
            }

            steps {
                withCredentials([usernamePassword(
                    credentialsId: "registry", usernameVariable: "USER", passwordVariable: "PASS")]) {
                        sh 'echo "### Deploying..."'
                        sh """
                        if [ ! -d "/tmp/deploy" ] ; then
                            git clone git@gitlab.com:mehdiHadji/tooling_deploy_playbook.git /tmp/deploy
                        else
                            cd /tmp/deploy
                            git pull
                        fi
                        ansible-playbook /tmp/deploy/python_service/site.yml -i /tmp/deploy/python_service/inventory/dev --extra-vars "ansible_sudo_pass=underworld profile=dev registry_password=$PASS registry_user=$USER img_version=${COMMIT_HASH}"
                        """
                 }


            }
        }
    }
}



def releaseCurrentVersion() {
    input 'Are you sure to Release ?'
    def releaseVersion = input id: 'releaseVersionID', message: 'Specify version', parameters: [string(defaultValue: '', description: 'write version like M.m.p ex: 1.0.0', name: 'Release Version! Leave empty to disable release')]
    env.TARGET = input message: 'please select target deployment env', parameters: [choice(name: 'targetEnv', choices: 'dev\nprod\npreprod', description: 'Target deployment env')]
    if (releaseVersion?.trim()) {
        prepareAndDeploy(releaseVersion)
    } else {
        echo 'Release Aborted! No specified Release Version'
    }
}

def prepareAndDeploy(newVersion) {
    def CONTAINER_REGISTRY = 'registry.gitlab.com/mehdihadji/python_service'
    echo '######### deploy To Registry'

    withCredentials([usernamePassword(
                    credentialsId: "registry", usernameVariable: "USER", passwordVariable: "PASS")]) {
                        sh "echo $PASS | docker login registry.gitlab.com -u $USER --password-stdin"
                }

    sh "docker tag ${CONTAINER_REGISTRY}:latest ${CONTAINER_REGISTRY}:${newVersion}-${env.TARGET}"
    sh "docker push ${CONTAINER_REGISTRY}:${newVersion}-${env.TARGET}"
    echo '########## branching Tag'

    sh "git tag -a \"${newVersion}-${env.TARGET}\" -m \"Tagging version ${newVersion}-${env.TARGET}\""

    sh 'git push --tags'
}
