# Run: docker-compose build
```shell script
docker-compose up -d --build
```

# Stop
```shell script
docker-compose stop
```

or clean up with:
```shell script
./cleanup.sh
```